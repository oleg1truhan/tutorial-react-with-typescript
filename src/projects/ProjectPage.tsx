import React, { useEffect, useState } from 'react';
import { Project } from './Project';
import { projectAPI } from './projectAPI';
import { useParams } from 'react-router-dom';
import ProjectDetail from './ProjectDetail';

const ProjectsPage = () => {
    const [ loading, setLoading ] = useState(false);
    const [ project, setProject ] = useState<Project | null>(null);
    const [ error, setError ] = useState<string | null>(null);
    const params = useParams();
    const id = Number(params.id);

    useEffect(() => {
        setLoading(true);
        projectAPI.find(id)
            .then((data) => setProject(data))
            .catch((e: any) => setError(e))
            .finally(() => setLoading(false));
    }, [ id ]);

    return <>
        <h1>Projects</h1>

        {loading && (
            <div className="center-page">
                <span className="spinner primary"></span>
                <p>Loading...</p>
            </div>
        )}

        {error && (
            <div className="row">
                <div className="card large error">
                    <section>
                        <p>
                            <span className="icon-alert inverse"></span>
                            {error}
                        </p>
                    </section>
                </div>
            </div>
        )}

        {project && <ProjectDetail project={project}/>}


    </>;
};

export default ProjectsPage;
