import React, { useEffect } from 'react';
import ProjectList from './ProjectList';
import { useSelector } from 'react-redux';
import { useActions } from '../hooks/useActions';
import { getError, getLoading, getPage, getProjects } from '../store/selectors';

const ProjectsPage = () => {
    const loading = useSelector(getLoading);
    const projects = useSelector(getProjects);
    const error = useSelector(getError);
    const currentPage = useSelector(getPage);
    const { loadProjects } = useActions();

    useEffect(() => {
        loadProjects(1);
    }, []);

    const handleMoreClick = () =>
        loadProjects(currentPage + 1);

    return <>
        <h1>Projects</h1>

        {error && (
            <div className="row">
                <div className="card large error">
                    <section>
                        <p>
                            <span className="icon-alert inverse"></span>
                            {error}
                        </p>
                    </section>
                </div>
            </div>
        )}

        <ProjectList
            projects={projects}
        />

        {!loading && !error && (
            <div className="row">
                <div className="col-sm-12">
                    <div className="button-group fluid">
                        <button
                            className="button default"
                            onClick={handleMoreClick}
                        >
                            More...
                        </button>
                    </div>
                </div>
            </div>
        )}

        {loading && (
            <div className="center-page">
                <span className="spinner primary"></span>
                <p>Loading...</p>
            </div>
        )}

    </>;
};

export default ProjectsPage;
