import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import { Project } from './Project';

const formatDescription = (description: string): string => {
    return description.substring(0, 60) + '...';
};

interface IProps {
    project: Project;
    onEdit: (project: Project) => void;
}

const ProjectCard: FC<IProps> = ({ project, onEdit }) => {
    const handleClick = (projectBeingEdit: Project) => {
        onEdit(projectBeingEdit);
    };

    return <div className="card">
        <img src={project.imageUrl} alt="project name"/>
        <section className="section dark">
            <Link to={`/projects/` + project.id}>
                <h5 className="strong">
                    <strong>{project.name}</strong>
                </h5>
                <p>{formatDescription(project.description)}</p>
                <p>Budget : {project.budget.toLocaleString()}</p>
            </Link>
            <button
                aria-label={`edit ${project.name}`}
                className="bordered"
                onClick={() => handleClick(project)}
            >
                <span className="icon-edit"></span>
                Edit
            </button>
        </section>
    </div>;
};

export default ProjectCard;
