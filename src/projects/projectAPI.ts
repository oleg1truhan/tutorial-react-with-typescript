import { Project } from './Project';

const baseUrl = 'http://localhost:4000';
export const url = `${baseUrl}/projects`;

const translateStatusToErrorMessage = (status: number) => {
    switch (status) {
        case 401:
            return 'Please login again';
        case 403:
            return 'You do not have permission to view the project(s).';
        default:
            return 'There was an error retrieving the project(s). Please try again.';
    }
};

const checkStatus = (response: any) => {
    if (response.ok) {
        return response;
    } else {
        const httpErrorInfo = {
            status: response.status,
            statusText: response.statusText,
            url: response.url,
        };
        console.log(`log server http error: ${JSON.stringify(httpErrorInfo)}`);

        let errorMessage = translateStatusToErrorMessage(httpErrorInfo.status);
        throw new Error(errorMessage);
    }
};

const parseJSON = (response: Response) => response.json();

const delay = (ms: number) => (x: any): Promise<any> => (
    new Promise((resolve => setTimeout(() => resolve(x), ms))));

const convertToProjectModel = (item: any): Project => new Project(item);

const convertToProjectModels = (data: any[]): Project[] => {
    let projects: Project[] = data.map(convertToProjectModel);
    return projects;
};

export const projectAPI = {
    get: (page = 1, limit = 20) =>
        fetch(`${url}?_page=${page}&_limit=${limit}&_sort=name`)
            .then(checkStatus)
            .then(parseJSON)
            .then(convertToProjectModels)
            .catch((error: TypeError) => {
                console.log('log client error ' + error);
                throw new Error(
                    'There was an error retrieving the projects. Please try again.'
                );
            }),
    put: (project: Project) =>
        fetch(`${url}/${project.id}`, {
            method: 'PUT',
            body: JSON.stringify(project),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(checkStatus)
            .then(parseJSON)
            .catch((error: TypeError) => {
                console.log('log client error ' + error);
                throw new Error(
                    'There was an error updating the projects. Please try again.'
                );
            }),
    find: (id: number) => fetch(`${url}/${id}`)
        .then(checkStatus)
        .then(parseJSON)
        .then(convertToProjectModel),
};
