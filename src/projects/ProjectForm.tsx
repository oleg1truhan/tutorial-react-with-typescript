import React, { FC, SyntheticEvent, useState } from 'react';
import { Project } from './Project';
import { useActions } from '../hooks/useActions';

interface IProps {
    project: Project;
    onCancel: () => void;
}

interface IError {
    name: string;
    description: string;
    budget: string;
}

const ProjectForm: FC<IProps> = ({ project: initialProject, onCancel }) => {
    const { saveProject } = useActions();
    const [ project, setProject ] = useState(initialProject);
    const [ errors, setErrors ] = useState({
            name: '',
            description: '',
            budget: ''
        }
    );

    const validate = (project: Project) => {
        let errors: IError = { name: '', description: '', budget: '' };
        if (project.name.length === 0) errors.name = 'Name is required.';
        if (project.name.length > 0 && project.name.length < 3) errors.name = 'Name needs to be at least 3 characters.';
        if (project.description.length === 0) errors.name = 'Description is required.';
        if (project.budget <= 0) errors.name = 'Budget must be more than $0.';

        return errors;
    };

    const isValid = () => {
        return (
            errors.description.length === 0 &&
            errors.name.length === 0 &&
            errors.budget.length === 0
        );
    };

    const handleChange = (e: any) => {
        e.preventDefault();
        const { type, name, value, checked } = e.target;

        let updatedValue = type === 'checkbox' ? checked : value;

        if (type === 'number') updatedValue = Number(updatedValue);

        const change = { [name]: updatedValue };
        let updatedProject: Project;

        setProject((p) => {
            updatedProject = new Project({ ...p, ...change });
            return updatedProject;
        });
        setErrors(() => validate(updatedProject));
    };

    const handleSubmit = (e: SyntheticEvent) => {
        e.preventDefault();
        if (!isValid()) return;
        saveProject(project);
    };

    return <form
        aria-label="Edit a Project"
        name="projectForm"
        className="input-group vertical"
        onSubmit={handleSubmit}
    >
        {errors.name.length > 0 && (
            <div
                role="alert"
                className="card error"
            >
                <p>{errors.name}</p>
            </div>
        )}
        <label htmlFor="name">Project Name</label>
        <input
            id="name"
            aria-label="project name"
            type="text"
            name="name"
            placeholder="Enter name"
            value={project.name}
            onChange={handleChange}
        />

        <label htmlFor="description">Project Description</label>
        <textarea
            id="description"
            aria-label="project description"
            name="description"
            placeholder="Enter description"
            value={project.description}
            onChange={handleChange}
        ></textarea>
        {errors.description.length > 0 && (
            <div
                role="alert"
                className="card error"
            >
                <p>{errors.description}</p>
            </div>
        )}

        <label htmlFor="budget">Project Budget</label>
        <input
            id="budget"
            type="number"
            name="budget"
            placeholder="Enter budget"
            value={project.budget}
            onChange={handleChange}
        />
        {errors.budget.length > 0 && (
            <div
                role="alert"
                className="card error"
            >
                <p>{errors.budget}</p>
            </div>
        )}

        <label htmlFor="isActive">Active?</label>
        <input
            id="isActive"
            type="checkbox"
            name="isActive"
            checked={project.isActive}
            onChange={handleChange}
        />

        <div className="input-group">
            <button
                className="primary bordered medium"
            >Save
            </button>
            <span></span>
            <button
                className="bordered medium"
                onClick={onCancel}
            >Cancel
            </button>
        </div>
    </form>;
};

export default ProjectForm;
