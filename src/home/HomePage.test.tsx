import React from 'react';
import { render, screen } from '@testing-library/react';
import rerender from 'react-test-renderer';
import HomePage from './HomePage';

describe('<HomePage />', () => {
    test('renders home heading', () => {
        render(<HomePage/>);
        expect(screen.getByRole('heading')).toHaveTextContent('Home');
    });

    test('snapshot', () => {
        const tree = rerender.create(<HomePage/>).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
