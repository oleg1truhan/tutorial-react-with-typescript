import { AppState } from './store';

export const getLoading = (state: AppState) => state.project.loading;
export const getProjects = (state: AppState) => state.project.projects;
export const getError = (state: AppState) => state.project.error;
export const getPage = (state: AppState) => state.project.page;
