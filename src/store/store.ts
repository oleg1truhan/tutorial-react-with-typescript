import { applyMiddleware, combineReducers, createStore } from 'redux';
import ReduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { initialProjectState, projectReducer } from './projectReducer';

const rootReducer = combineReducers({
    project: projectReducer
});

export default function configureStore(preloadState: any) {
    const middlewares = [ReduxThunk];
    const middlewareEnhance = applyMiddleware(...middlewares);

    const enhancer = composeWithDevTools(middlewareEnhance);
    const store = createStore(rootReducer, preloadState, enhancer);
    return store;
};

type RootReduce = typeof rootReducer;
export type AppState = ReturnType<RootReduce>;

export const initialAppState: AppState = {
    project: initialProjectState
};

export const store = configureStore(initialAppState);
