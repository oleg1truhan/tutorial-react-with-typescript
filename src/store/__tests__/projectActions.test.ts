import configureMockStore from 'redux-mock-store';
import ReduxThunk from 'redux-thunk';
import { initialAppState } from '../store';
import { LOAD_PROJECTS_FAILURE, LOAD_PROJECTS_REQUEST, LOAD_PROJECTS_SUCCESS } from '../projectTypes';
import { MOCK_PROJECTS } from '../../projects/MockProjects';
import { projectActions } from '../projectsActions';
import { projectAPI } from '../../projects/projectAPI';

jest.mock('../../projects/projectAPI');

const middlewares = [ ReduxThunk ];
const mockStoreCreator = configureMockStore(middlewares);

describe('Project Actions', () => {
    let store: any;

    beforeEach(() => {
        store = mockStoreCreator(initialAppState);
    });

    test('should load projects successfully', () => {
        const expectedActions = [
            { type: LOAD_PROJECTS_REQUEST }, {
                type: LOAD_PROJECTS_SUCCESS,
                payload: { projects: MOCK_PROJECTS, page: 1 }
            }
        ];

        return store.dispatch(projectActions.loadProjects(1)).then(() => {
            const actions = store.getActions();
            expect(actions).toEqual(expectedActions);
        });
    });

    test('should return error', () => {
       projectAPI.get = jest.fn()
        .mockImplementationOnce(() => Promise.reject('failed'));

        const expectedActions = [
            { type: LOAD_PROJECTS_REQUEST }, {
                type: LOAD_PROJECTS_FAILURE,
                payload: 'failed'
            }
        ];

        return store.dispatch(projectActions.loadProjects(1)).then(() => {
            const actions = store.getActions();
            expect(actions).toEqual(expectedActions);
        });
    });
});
