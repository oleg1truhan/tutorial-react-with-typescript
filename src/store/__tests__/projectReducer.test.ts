import { MOCK_PROJECTS } from '../../projects/MockProjects';
import { Project } from '../../projects/Project';
import { initialProjectState, projectReducer } from '../projectReducer';
import { ProjectState, SAVE_PROJECT_SUCCESS } from '../projectTypes';

describe('project reducer', () => {
    test('should update an existing project', () => {
        const project = MOCK_PROJECTS[0];
        const updatedProject = Object.assign(new Project(), project, {
            name: project.name + ' updated'
        });
        const currentState: ProjectState = { ...initialProjectState, projects: [project] };
        const updatedState: ProjectState = { ...initialProjectState, projects: [ updatedProject ] };

        expect(projectReducer(currentState, {
            type: SAVE_PROJECT_SUCCESS,
            payload: updatedProject
        })).toEqual(updatedState);
    });
});
