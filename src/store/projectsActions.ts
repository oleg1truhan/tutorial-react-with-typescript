import { Dispatch } from 'redux';
import {
    LOAD_PROJECTS_FAILURE,
    LOAD_PROJECTS_REQUEST,
    LOAD_PROJECTS_SUCCESS,
    ProjectActionTypes,
    SAVE_PROJECT_FAILURE,
    SAVE_PROJECT_REQUEST,
    SAVE_PROJECT_SUCCESS
} from './projectTypes';
import { projectAPI } from '../projects/projectAPI';
import { Project } from '../projects/Project';

export const projectActions = {
    loadProjects: (page: number) =>
        async (dispatch: Dispatch<ProjectActionTypes>) => {
            try {
                dispatch({ type: LOAD_PROJECTS_REQUEST });
                const data = await projectAPI.get(page);
                dispatch({
                    type: LOAD_PROJECTS_SUCCESS,
                    payload: { projects: data, page: page },
                });
            } catch (error: any) {
                dispatch({ type: LOAD_PROJECTS_FAILURE, payload: error });
            }
        },
    saveProject: (project: Project) =>
        async (dispatch: Dispatch<ProjectActionTypes>) => {
            try {
                dispatch({ type: SAVE_PROJECT_REQUEST });
                const data = await projectAPI.put(project);
                dispatch({
                    type: SAVE_PROJECT_SUCCESS,
                    payload: data,
                });
            } catch (error: any) {
                dispatch({ type: SAVE_PROJECT_FAILURE, payload: error });
            }
        }
};

