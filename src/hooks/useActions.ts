import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { projectActions } from '../store/projectsActions';

export const useActions = () => {
    const dispatch = useDispatch();
    return bindActionCreators(projectActions, dispatch);
};
